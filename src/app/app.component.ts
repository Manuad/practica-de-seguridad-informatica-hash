import { Component, OnInit } from '@angular/core';
import { LocalService } from './services/local.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  constructor(private localService: LocalService) {

  }
  setLocalStorage(){
    let user = {
      name: 'Manuel',
      lastname: 'Carrillo'
    }
    this.localService.setJsonValue('user', user);
  }
  getLocalStorage() {
    let userdata = this.localService.getJsonValue('user');
    console.log(userdata);
  }
  ngOnInit() {
    this.setLocalStorage();
    this.getLocalStorage();
  }
  title = 'HASHING-ng';
}
