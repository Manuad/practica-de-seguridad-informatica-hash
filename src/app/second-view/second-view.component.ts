import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms'
import { StorageService } from '../services/storage.service';
import { LocalService } from '../services/local.service'
import sha256 from 'crypto-js/sha256';

@Component({
  selector: 'app-second-view',
  templateUrl: './second-view.component.html',
  styleUrls: ['./second-view.component.css']
})
export class SecondViewComponent {
  
  encryptForm = new FormGroup({
    message: new FormControl('', Validators.required)
  });
  itsGood = false;
  itsWrong = false;
  itsEnough = false;
  rigthMessage = false;
  count = 1;
  
  constructor(private storageService: StorageService, private localService: LocalService) {
  }
  validateMessage() {
    try {
      const encripted = 'e8d21a7eb4644af714eb33238ecae5f9d38afc749373d96baa0f97d39c823f78';
      const message = this.encryptForm.value.message;
      this.storageService.secureStorage.hash(message);
      console.log(this.storageService.secureStorage.hash('mensaje encriptado por servicio: ' + message));
      
      if(this.storageService.secureStorage.hash(message) === encripted){
        console.log('Este es el mensaje del usuario encriptado' + message);
        console.log('Este es el texto encriptado' + encripted);
        this.itsGood = true;
        this.itsEnough = false;
        this.itsWrong = false;
      }
      else if(this.storageService.secureStorage.hash(message) !== encripted) {
        if(this.count <= 3){
          this.itsWrong = true;
          this.itsGood = false;
          this.itsEnough = false;
          this.encryptForm.reset();
        }
        if(this.count === 3) {
          this.itsEnough = true;
          this.itsWrong = false;
          this.itsGood = false;
          this.rigthMessage = true;
          this.encryptForm.disable();
          this.encryptForm.reset();
        }
        this.count++;
      }
    } catch (error) {
      console.log(error)
    }
  }
  onSubmit(){
     this.validateMessage();
   }

}
