import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { StorageService } from '../services/storage.service';
import { LocalService } from '../services/local.service'

@Component({
  selector: 'app-first-view',
  templateUrl: './first-view.component.html',
  styleUrls: ['./first-view.component.css']
})
export class FirstViewComponent {

  hashForm = new FormGroup({
    title: new FormControl('', Validators.required),
    message: new FormControl('', Validators.required)
  });

  constructor(private storageService: StorageService, private localService: LocalService) {

  }
  setLocalStorage(){
    let hashMessage = {
      title: this.hashForm.value.title,
      message: this.hashForm.value.message
    }
    this.localService.setJsonValue('hashMessage', 
      this.storageService.secureStorage.hash(hashMessage)
      );
    this.storageService.secureStorage.hash(hashMessage);
    console.log(this.storageService.secureStorage.hash(hashMessage));
  }
  getLocalStorage() {
    let hashData = this.localService.getJsonValue('hashMessage');
    console.log(hashData);
  }
  onSubmit(){
    this.setLocalStorage();
    this.getLocalStorage();
  }
}
